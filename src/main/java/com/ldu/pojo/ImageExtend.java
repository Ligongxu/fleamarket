package com.ldu.pojo;

import java.util.ArrayList;
import java.util.List;

public class ImageExtend {
    private List<Image> images = new ArrayList<Image>();

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }
    @Override
    public String toString() {
        return "ImageExtend [ images=" + images + "]";
    }

}
